package middleware

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
)

func HTTPError(ctx *fiber.Ctx, err error) error {
	type res struct {
		Message string `json:"message"`
	}
	ctx.Status(http.StatusInternalServerError)
	return ctx.JSON(res{Message: err.Error()})
}
