package handler

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/general-sandra/layered-architecture/rest/handler/greetingHandler"
)

func GroupHandle(app *fiber.App) {
	g := app.Group("/")
	greetingHandler.Route(g)
}
