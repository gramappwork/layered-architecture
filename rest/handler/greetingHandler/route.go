package greetingHandler

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/general-sandra/layered-architecture/rest/service/greeting"
)

func Route(r fiber.Router) {
	r.Get("greeting", greeting.Greeting)
}
