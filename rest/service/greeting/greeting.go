package greeting

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	"os"
)

func Greeting(ctx *fiber.Ctx) error {
	ctx.Status(http.StatusOK)
	s, _ := os.LookupEnv("HOSTNAME")
	s += s + " say " + "hello"
	return ctx.SendString(s)
}
