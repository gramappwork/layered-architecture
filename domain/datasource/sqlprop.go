package datasource

type OracleProp struct {
	Host        string `yaml:"host,omitempty"`
	Port        int    `yaml:"port,omitempty"`
	Driver      string `yaml:"driver,omitempty"`
	Sid         string `yaml:"sid,omitempty"`
	Password    string `yaml:"password,omitempty"`
	Username    string `yaml:"username,omitempty"`
	MaxOpenConn int    `yaml:"maxOpenConn,omitempty"`
	MaxIdleConn int    `yaml:"maxIdleConn,omitempty"`
}
