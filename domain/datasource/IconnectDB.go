package datasource

import "database/sql"

type IConnectRDBMS interface {
	OpenConn(p OracleProp) (*sql.DB, error)
}
