package app

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Properties struct {
	Viper   *viper.Viper
	DB      *sql.DB
	Log     *logrus.Logger
	Message *viper.Viper
}
