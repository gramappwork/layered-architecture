package messageReader

import (
	"github.com/spf13/viper"
)

const (
	filename   = "messages"
	fileExtend = "yml"
)

var Path = "."

func NewMsg() (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigName(filename)
	v.SetConfigType(fileExtend)
	v.AddConfigPath(Path)
	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	return v, nil
}
