package oracleConnecting

import (
	"database/sql"
	"fmt"
	"gitlab.com/general-sandra/layered-architecture/domain/datasource"
)

func New() datasource.IConnectRDBMS {
	return &connector{}
}

type connector struct {
}

func (c connector) OpenConn(prop datasource.OracleProp) (*sql.DB, error) {
	url := fmt.Sprintf("%s://%s:%s@%s:%d/%s", prop.Driver, prop.Username, prop.Password, prop.Host, prop.Port, prop.Sid)
	db, err := sql.Open(prop.Driver, url)
	if err != nil {
		return nil, err
	}
	mic := prop.MaxIdleConn
	moc := prop.MaxOpenConn
	db.SetMaxIdleConns(mic)
	db.SetMaxOpenConns(moc)
	if err := db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
