package configReader

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

const (
	filename   = "config"
	fileExtend = "yml"
)

var Path = "."

func readConfDefault() (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigName(filename)
	v.SetConfigType(fileExtend)
	v.AddConfigPath(Path)
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	return v, nil
}

func NewConf() (*viper.Viper, error) {
	v, err := readConfDefault()
	if err != nil {
		return nil, err
	}
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	fileEnv := v.GetString("spec.use")
	fileEnv = fmt.Sprintf("%s.%s", fileEnv, fileExtend)
	if _, err := os.Stat(filepath.Join(Path, fileEnv)); err != nil {
		return nil, err
	}

	v.SetConfigName(fileEnv)
	v.SetConfigType(fileExtend)
	v.AddConfigPath(Path)
	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	return v, nil
}
