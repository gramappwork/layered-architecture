package main

import (
	"database/sql"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	_ "github.com/sijms/go-ora/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/general-sandra/layered-architecture/adapter/configReader"
	"gitlab.com/general-sandra/layered-architecture/adapter/messageReader"
	"gitlab.com/general-sandra/layered-architecture/adapter/repository/oracleConnecting"
	"gitlab.com/general-sandra/layered-architecture/di"
	"gitlab.com/general-sandra/layered-architecture/domain/app"
	"gitlab.com/general-sandra/layered-architecture/domain/datasource"
	"gitlab.com/general-sandra/layered-architecture/rest/handler"
	"gitlab.com/general-sandra/layered-architecture/rest/middleware"
	"os"
	"os/signal"
	"syscall"
	_ "time/tzdata"
)

var prop = &app.Properties{}

func bootstrap() error {

	conf, err := readConf()
	if err != nil {
		return err
	}
	prop.Viper = conf

	msg, err := readMsg()
	if err != nil {
		return err
	}
	prop.Message = msg

	logger, err := setupLog(conf)
	if err != nil {
		return err
	}
	prop.Log = logger

	if !isIgnoreDB(conf) {

		//DataMartDB Instance
		db, err := openConnOracle(conf)
		if err != nil {
			return err
		}
		prop.DB = db
	}
	di.Inject(prop)
	return nil
}

func main() {
	if err := bootstrap(); err != nil {
		panic(err)
	}
	app := fiber.New(
		fiber.Config{
			Prefork:      prop.Viper.GetBool("app.prefork"),
			ErrorHandler: middleware.HTTPError,
		},
	)

	app.Use(cors.New())

	app.Use(pprof.New())
	address := "%s:%d"
	handler.GroupHandle(app)
	address = fmt.Sprintf(address, prop.Viper.GetString("app.host"), prop.Viper.GetInt("app.port"))
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	go graceFulShutDown(c, app)

	if err := app.Listen(address); err != nil {
		prop.Log.Panic(err)
	}

	prop.Log.Infoln("Running cleanup tasks...")
}

func readConf() (*viper.Viper, error) {
	v, err := configReader.NewConf()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func readMsg() (*viper.Viper, error) {
	return messageReader.NewMsg()
}

func openConnOracle(conf *viper.Viper) (*sql.DB, error) {
	sqlConf := datasource.OracleProp{
		Host:        conf.GetString("datasource.oracle.host"),
		Port:        conf.GetInt("datasource.oracle.port"),
		Driver:      conf.GetString("datasource.oracle.driver"),
		Sid:         conf.GetString("datasource.oracle.sid"),
		Password:    conf.GetString("datasource.oracle.password"),
		Username:    conf.GetString("datasource.oracle.username"),
		MaxIdleConn: conf.GetInt("datasource.oracle.maxIdleConn"),
		MaxOpenConn: conf.GetInt("datasource.oracle.maxOpenConn"),
	}
	ora := oracleConnecting.New()
	return ora.OpenConn(sqlConf)
}

func setupLog(conf *viper.Viper) (*logrus.Logger, error) {
	l := logrus.New()
	lvl, err := logrus.ParseLevel(conf.GetString("log.level"))
	if err != nil {
		return nil, err
	}
	l.Level = lvl
	l.ReportCaller = conf.GetBool("log.reportCaller")
	l.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: conf.GetBool("log.prettyPrint"),
	})
	return l, nil
}

func isIgnoreDB(conf *viper.Viper) bool {
	return conf.GetBool("datasource.ignore") == true
}

func graceFulShutDown(c chan os.Signal, app *fiber.App) {
	go func() {
		_ = <-c
		prop.Log.Infoln("Gracefully shutting down...")
		if err := app.Shutdown(); err != nil {
			prop.Log.Errorln(err)
		}
		if !prop.Viper.GetBool("datasource.ignore") {
			if err := prop.DB.Close(); err != nil {
				prop.Log.Errorln(err)
			}
		}
	}()
}
