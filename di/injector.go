package di

import (
	"gitlab.com/general-sandra/layered-architecture/adapter"
	"gitlab.com/general-sandra/layered-architecture/domain"
	"gitlab.com/general-sandra/layered-architecture/domain/app"
	"gitlab.com/general-sandra/layered-architecture/rest/middleware"
	"gitlab.com/general-sandra/layered-architecture/rest/service"
)

func Inject(ap *app.Properties) {
	service.InjectProperties(ap)
	domain.InjectProperties(ap)
	adapter.InjectProperties(ap)
	middleware.InjectProperties(ap)
}
